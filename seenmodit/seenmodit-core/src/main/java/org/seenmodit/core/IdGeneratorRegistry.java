/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seenmodit.core;

import org.seenmodit.core.generators.IdGeneratorMongoHexString;
import org.seenmodit.core.interfaces.IdGenerator;

/**
 * This is registry for IdGenerators. There is default set whi geenrates String
 * id from ObjectId (mongo 24 hex digit id)
 *
 * @author xnet
 */
public class IdGeneratorRegistry {

    private static IdGenerator DEFAULT_GENERATOR = new IdGeneratorMongoHexString();
    private static IdGenerator GENERATOR = DEFAULT_GENERATOR;

    public static void restoreDefaultIdGenerator() {
        GENERATOR = DEFAULT_GENERATOR;
    }

    public static void setIdGenerator(IdGenerator generator) {
        GENERATOR = generator;
    }

    public static IdGenerator getIdGenerator() {
        return GENERATOR;
    }

}
