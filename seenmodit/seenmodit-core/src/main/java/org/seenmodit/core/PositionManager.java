/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.seenmodit.core.exceptions.PositionManagerException;
import org.seenmodit.core.exceptions.model.Pair;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import org.seenmodit.core.interfaces.IdGenerator;
import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;

/**
 *
 * @author jknast
 */
public class PositionManager {

    private final IdGenerator idGenerator;

    /**
     * Creates certain PositionManager with given IdGenerator implementation
     *
     * @param idGenerator
     * @return
     */
    public static PositionManager create(final IdGenerator idGenerator) {
        return new PositionManager(idGenerator);
    }

    /**
     *
     * Creates certain PositionManager IdGenerator from IdGeneratorRegistry. To
     * register IdGenerator run IdGeneratorRegistry.setIdGenerator(generator)
     *
     * @return
     */
    public static PositionManager create() {
        return new PositionManager(IdGeneratorRegistry.getIdGenerator());
    }

    private PositionManager(final IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public <E extends Position, U extends UpdatePosition> PositionsChangesRet<E, U> findAddedRemovedUpdated(
            List<E> actualsOrderPositions, List<U> updateOrderPositions) throws PositionManagerException {
        return findAddedRemovedUpdated(actualsOrderPositions, updateOrderPositions, true);
    }

    public <E extends Position, U extends UpdatePosition> PositionsChangesRet<E, U> findAddedRemovedUpdated(
            List<E> actualsOrderPositions, List<U> updateOrderPositions, boolean addIdsToNewItems) {

        PositionsChangesRet<E, U> ret = new PositionsChangesRet();

        //prepare temp helping orderPositionIds list
        Map<String, E> actualsMap = new HashMap();
        Set<String> updatedIdSet = new HashSet();

        for (final E e : actualsOrderPositions) {
            actualsMap.put(e.getPositionId(), e);
        }
        if (actualsOrderPositions.size() != actualsMap.size()) {
            throw new PositionManagerException("In positions in DB there are doubled positionIds");
        }
        for (final U updateOrderPosition : updateOrderPositions) {
            String updatesPosId = updateOrderPosition.getPositionId();
            if (isNullOrEmpty(updatesPosId)) {
                //There is newely added position, without ID. Add ID if addIdsToNewItems is true

                if (idGenerator == null) {
                    throw new PositionManagerException("You requested for adding ids to new items, but IdGenerator is null - not specified?? Use PositionManager.create(idGenerator)");
                }
                updateOrderPosition.setPositionId(idGenerator.nextId());
                ret.getAdded().add(updateOrderPosition);
            }else if (!actualsMap.containsKey(updatesPosId)) {
                //There is new position but it has id
                ret.getAdded().add(updateOrderPosition);                
            } else {
                // this position exists in db and in request
                E old = actualsMap.get(updatesPosId);
                ret.getOther().add(new Pair(old, updateOrderPosition));
            }
        }

        for (final U u : updateOrderPositions) {
            updatedIdSet.add(u.getPositionId());
        }
        if (updateOrderPositions.size() != updatedIdSet.size()) {
            throw new PositionManagerException("In positions from request there are doubled positionIds");
        }

        //trzeba usunąć produkty, z usuniętych pozycji na zamówieniu
        // had to remove items from removed
        for (E actualOrderPosition : actualsOrderPositions) {
            String actualOrderPositionId = actualOrderPosition.getPositionId();
            if (!updatedIdSet.contains(actualOrderPositionId)) {
                // jeśli nowa lista nie zawiera elementu starej, to znaczy ze element usunięty
                ret.getRemoved().add(actualOrderPosition);
            }
        }
        if (!addIdsToNewItems) {
            ret.removeIdsFromAdded();
        }
        return ret;
    }

    private boolean isNullOrEmpty(String positionId) {
        return positionId == null || positionId.isEmpty();
    }
}
