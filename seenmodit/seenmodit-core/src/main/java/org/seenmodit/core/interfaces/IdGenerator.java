/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.core.interfaces;

/**
 *
 * @author jknast
 */
public interface IdGenerator {
    
    String nextId();
}
