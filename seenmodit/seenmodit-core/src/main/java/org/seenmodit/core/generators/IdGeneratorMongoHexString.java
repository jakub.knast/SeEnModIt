/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seenmodit.core.generators;

import org.bson.types.ObjectId;
import org.seenmodit.core.interfaces.IdGenerator;

/**
 *
 * @author xnet
 */
public class IdGeneratorMongoHexString implements IdGenerator {

    @Override
    public String nextId() {
        return new ObjectId().toHexString();
    }

}
