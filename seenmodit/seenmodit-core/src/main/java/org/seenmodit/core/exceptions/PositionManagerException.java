/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.core.exceptions;

/**
 *
 * @author jknast
 */
public class PositionManagerException extends RuntimeException {

    public PositionManagerException(String message) {
        super(message);
    }

    public PositionManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public PositionManagerException(Throwable cause) {
        super(cause);
    }

    public PositionManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
