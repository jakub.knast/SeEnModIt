/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.core.exceptions.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.seenmodit.core.exceptions.PositionManagerException;
import org.seenmodit.core.interfaces.IdGenerator;
import org.seenmodit.core.interfaces.Position;
import org.seenmodit.core.interfaces.UpdatePosition;

/**
 * @author jknast
 *
 */
public class PositionsChangesRet<ACTUAL_CLASS extends Position, UPDATED_CLASS extends UpdatePosition> {

    private List<UPDATED_CLASS> added = new ArrayList();
    private List<ACTUAL_CLASS> removed = new ArrayList();
    private List<Pair<ACTUAL_CLASS, UPDATED_CLASS>> other = new ArrayList();
    private IdGenerator idGenerator;

    public PositionsChangesRet() {
    }

    public PositionsChangesRet(List<UPDATED_CLASS> added, List<ACTUAL_CLASS> removed, List<Pair<ACTUAL_CLASS, UPDATED_CLASS>> other, IdGenerator idGenerator) {
        this.added = added;
        this.removed = removed;
        this.other = other;
        this.idGenerator = idGenerator;
    }

    public List<UPDATED_CLASS> getAdded() {
        return added;
    }

    public void setAdded(List<UPDATED_CLASS> added) {
        this.added = added;
    }

    public List<ACTUAL_CLASS> getRemoved() {
        return removed;
    }

    public void setRemoved(List<ACTUAL_CLASS> removed) {
        this.removed = removed;
    }

    public List<Pair<ACTUAL_CLASS, UPDATED_CLASS>> getOther() {
        return other;
    }

    public LinkedHashMap<ACTUAL_CLASS, UPDATED_CLASS> getOtherAsMap() {
        LinkedHashMap<ACTUAL_CLASS, UPDATED_CLASS> ret = new LinkedHashMap();
        for (Pair<ACTUAL_CLASS, UPDATED_CLASS> pair : other) {
            ret.put(pair.getFirst(), pair.getSecond());
        }
        return ret;
    }

    public void setOther(List<Pair<ACTUAL_CLASS, UPDATED_CLASS>> other) {
        this.other = other;
    }

    public List<ACTUAL_CLASS> getOriginFromOthers() {
        return this.other.stream()
                .map(pair -> pair.getFirst())
                .collect(Collectors.toList());
    }

    public List<UPDATED_CLASS> getUpdatedFromOthers() {
        return this.other.stream()
                .map(pair -> pair.getSecond())
                .collect(Collectors.toList());
    }

    public List<UPDATED_CLASS> setIdsToAdded() {
        if (idGenerator == null) {
            throw new PositionManagerException("You requested for adding ids to new items, but IdGenerator is null - not specified?? Use PositionManager.create(idGenerator)");
        }
        added.forEach((add) -> {
            add.setPositionId(idGenerator.nextId());
        });
        return added;
    }

    public List<UPDATED_CLASS> removeIdsFromAdded() {
        added.forEach((add) -> {
            add.setPositionId(null);
        });
        return added;
    }
}
