/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.core;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;
import org.seenmodit.core.exceptions.PositionManagerException;
import org.seenmodit.core.exceptions.model.Pair;
import org.seenmodit.core.exceptions.model.PositionsChangesRet;
import org.seenmodit.core.interfaces.IdGenerator;
import org.seenmodit.model.OrderEntity;
import org.seenmodit.model.UpdateOrderRequest;
import org.seenmodit.model.positions.OrderPosition;
import org.seenmodit.model.positions.UpdateOrderPosition;

/**
 *
 * @author jknast
 */
public class PositionManagerTest {

    @Before
    public void setUp() {
    }

    private static class NewOnesGenerator implements IdGenerator {

        int counter = 1;

        public NewOnesGenerator() {
        }

        @Override
        public String nextId() {
            return "new" + counter++;
        }
    }

    private <T extends Object> List<T> newArrayList(T... items) {
        List<T> ret = new ArrayList<>();
        for (T item : items) {
            ret.add(item);
        }
        return ret;
    }

    @Test
    public void testOnlyAdded() {
        // given
        List<OrderPosition> actual = newArrayList();
        List<UpdateOrderPosition> updated = newArrayList(
                new UpdateOrderPosition(null, "zachuczało"),
                new UpdateOrderPosition(null, "i"),
                new UpdateOrderPosition(null, "runął"),
                new UpdateOrderPosition(null, "dom"));
        OrderEntity actualEntity = new OrderEntity(actual);
        UpdateOrderRequest request = new UpdateOrderRequest(updated);

        PositionsChangesRet<OrderPosition, UpdateOrderPosition> expected = new PositionsChangesRet();
        expected.getAdded().add(new UpdateOrderPosition("new1", "zachuczało"));
        expected.getAdded().add(new UpdateOrderPosition("new2", "i"));
        expected.getAdded().add(new UpdateOrderPosition("new3", "runął"));
        expected.getAdded().add(new UpdateOrderPosition("new4", "dom"));

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        PositionsChangesRet<OrderPosition, UpdateOrderPosition> result = manager
                .findAddedRemovedUpdated(actualEntity.getOrderPositions(), request.getUpdateOrderPositions());

        // then
        assertThat(result.getAdded()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getAdded());
        assertThat(result.getRemoved()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getRemoved());
        assertThat(result.getOther()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getOther());
    }

    
    @Test
    public void testOnlyAddedWithIds() {
        // given
        List<OrderPosition> actual = newArrayList();
        List<UpdateOrderPosition> updated = newArrayList(
                new UpdateOrderPosition("1", "zachuczało"),
                new UpdateOrderPosition("2", "i"),
                new UpdateOrderPosition(null, "runął"),
                new UpdateOrderPosition(null, "dom"));
        OrderEntity actualEntity = new OrderEntity(actual);
        UpdateOrderRequest request = new UpdateOrderRequest(updated);

        PositionsChangesRet<OrderPosition, UpdateOrderPosition> expected = new PositionsChangesRet();
        expected.getAdded().add(new UpdateOrderPosition("1", "zachuczało"));
        expected.getAdded().add(new UpdateOrderPosition("2", "i"));
        expected.getAdded().add(new UpdateOrderPosition("new1", "runął"));
        expected.getAdded().add(new UpdateOrderPosition("new2", "dom"));

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        PositionsChangesRet<OrderPosition, UpdateOrderPosition> result = manager
                .findAddedRemovedUpdated(actualEntity.getOrderPositions(), request.getUpdateOrderPositions());

        // then
        assertThat(result.getAdded()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getAdded());
        assertThat(result.getRemoved()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getRemoved());
        assertThat(result.getOther()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getOther());
    }

    //krowy nocnych burz dały mleko
    @Test
    public void testOnlyAdded_WithoutSettingIds() {
        // given
        List<OrderPosition> actual = newArrayList();
        List<UpdateOrderPosition> updated = newArrayList(
                new UpdateOrderPosition(null, "zachuczało"),
                new UpdateOrderPosition(null, "i"),
                new UpdateOrderPosition(null, "runął"),
                new UpdateOrderPosition(null, "dom"));
        OrderEntity actualEntity = new OrderEntity(actual);
        UpdateOrderRequest request = new UpdateOrderRequest(updated);

        PositionsChangesRet<OrderPosition, UpdateOrderPosition> expected = new PositionsChangesRet();
        expected.getAdded().add(new UpdateOrderPosition(null, "zachuczało"));
        expected.getAdded().add(new UpdateOrderPosition(null, "i"));
        expected.getAdded().add(new UpdateOrderPosition(null, "runął"));
        expected.getAdded().add(new UpdateOrderPosition(null, "dom"));

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        PositionsChangesRet<OrderPosition, UpdateOrderPosition> result = manager
                .findAddedRemovedUpdated(actualEntity.getOrderPositions(), request.getUpdateOrderPositions(), false);

        // then
        assertThat(result.getAdded()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getAdded());
        assertThat(result.getRemoved()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getRemoved());
        assertThat(result.getOther()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getOther());
    }

    @Test
    public void testOnlyRemoved() {
        // given
        List<OrderPosition> actual = newArrayList(
                new OrderPosition("old1", "Skąd"),
                new OrderPosition("old2", "ja"),
                new OrderPosition("old3", "go"),
                new OrderPosition("old4", "znam?"));
        List<UpdateOrderPosition> updated = newArrayList();
        OrderEntity actualEntity = new OrderEntity(actual);
        UpdateOrderRequest request = new UpdateOrderRequest(updated);

        PositionsChangesRet<OrderPosition, UpdateOrderPosition> expected = new PositionsChangesRet();
        expected.getRemoved().add(new OrderPosition("old1", "Skąd"));
        expected.getRemoved().add(new OrderPosition("old2", "ja"));
        expected.getRemoved().add(new OrderPosition("old3", "go"));
        expected.getRemoved().add(new OrderPosition("old4", "znam?"));

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        PositionsChangesRet<OrderPosition, UpdateOrderPosition> result = manager.findAddedRemovedUpdated(actualEntity.getOrderPositions(), request.getUpdateOrderPositions());

        // then
        assertThat(result.getAdded()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getAdded());
        assertThat(result.getRemoved()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getRemoved());
        assertThat(result.getOther()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getOther());
    }

    @Test
    public void shall_add_and_update() {
        // given
        List<OrderPosition> actual = newArrayList(
                new OrderPosition("old1", "Zimny"),
                new OrderPosition("old2", "deszcz"),
                new OrderPosition("old3", "zmoczył"),
                new OrderPosition("old4", "jar"));
        List<UpdateOrderPosition> updated = newArrayList(
                new UpdateOrderPosition("old1", "Szary"),
                new UpdateOrderPosition("old2", "deszcz"),
                new UpdateOrderPosition("old3", "zmoczył"),
                new UpdateOrderPosition("old4", "jar"),
                new UpdateOrderPosition(null, "zachuczało"),
                new UpdateOrderPosition(null, "i"),
                new UpdateOrderPosition("blablabla", "runął"),
                new UpdateOrderPosition(null, "dom"));
        OrderEntity actualEntity = new OrderEntity(actual);
        UpdateOrderRequest request = new UpdateOrderRequest(updated);

        PositionsChangesRet<OrderPosition, UpdateOrderPosition> expected = new PositionsChangesRet();
        expected.getAdded().add(new UpdateOrderPosition("new1", "zachuczało"));
        expected.getAdded().add(new UpdateOrderPosition("new2", "i"));
        expected.getAdded().add(new UpdateOrderPosition("blablabla", "runął"));
        expected.getAdded().add(new UpdateOrderPosition("new3", "dom"));
        expected.getOther().add(new Pair<>(new OrderPosition("old1", "Zimny"), new UpdateOrderPosition("old1", "Szary")));
        expected.getOther().add(new Pair<>(new OrderPosition("old2", "deszcz"), new UpdateOrderPosition("old2", "deszcz")));
        expected.getOther().add(new Pair<>(new OrderPosition("old3", "zmoczył"), new UpdateOrderPosition("old3", "zmoczył")));
        expected.getOther().add(new Pair<>(new OrderPosition("old4", "jar"), new UpdateOrderPosition("old4", "jar")));

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        PositionsChangesRet<OrderPosition, UpdateOrderPosition> result = manager
                .findAddedRemovedUpdated(actualEntity.getOrderPositions(), request.getUpdateOrderPositions());

        // then
        assertThat(result.getAdded()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getAdded());
        assertThat(result.getRemoved()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getRemoved());
        assertThat(result.getOther()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getOther());
    }

    @Test
    public void shall_add_and_update_WithoutSettingIds() {
        // given
        List<OrderPosition> actual = newArrayList(
                new OrderPosition("old1", "Zimny"),
                new OrderPosition("old2", "deszcz"),
                new OrderPosition("old3", "zmoczył"),
                new OrderPosition("old4", "jar"));
        List<UpdateOrderPosition> updated = newArrayList(
                new UpdateOrderPosition("old1", "Szary"),
                new UpdateOrderPosition("old2", "deszcz"),
                new UpdateOrderPosition("old3", "zmoczył"),
                new UpdateOrderPosition("old4", "jar"),
                new UpdateOrderPosition(null, "zachuczało"),
                new UpdateOrderPosition(null, "i"),
                new UpdateOrderPosition(null, "runął"),
                new UpdateOrderPosition(null, "dom"));
        OrderEntity actualEntity = new OrderEntity(actual);
        UpdateOrderRequest request = new UpdateOrderRequest(updated);

        PositionsChangesRet<OrderPosition, UpdateOrderPosition> expected = new PositionsChangesRet();
        expected.getAdded().add(new UpdateOrderPosition(null, "zachuczało"));
        expected.getAdded().add(new UpdateOrderPosition(null, "i"));
        expected.getAdded().add(new UpdateOrderPosition(null, "runął"));
        expected.getAdded().add(new UpdateOrderPosition(null, "dom"));
        expected.getOther().add(new Pair<>(new OrderPosition("old1", "Zimny"), new UpdateOrderPosition("old1", "Szary")));
        expected.getOther().add(new Pair<>(new OrderPosition("old2", "deszcz"), new UpdateOrderPosition("old2", "deszcz")));
        expected.getOther().add(new Pair<>(new OrderPosition("old3", "zmoczył"), new UpdateOrderPosition("old3", "zmoczył")));
        expected.getOther().add(new Pair<>(new OrderPosition("old4", "jar"), new UpdateOrderPosition("old4", "jar")));

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        PositionsChangesRet<OrderPosition, UpdateOrderPosition> result = manager
                .findAddedRemovedUpdated(actualEntity.getOrderPositions(), request.getUpdateOrderPositions(), false);

        // then
        assertThat(result.getAdded()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getAdded());
        assertThat(result.getRemoved()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getRemoved());
        assertThat(result.getOther()).usingRecursiveFieldByFieldElementComparator().containsExactlyElementsOf(expected.getOther());
    }

    @Test
    public void throwBecouseDoubledIdsInDb() {
        // given
        List<OrderPosition> actual = newArrayList(
                new OrderPosition("doubled", "12"),
                new OrderPosition("doubled", "23223"));
        List<UpdateOrderPosition> updated = newArrayList();

        // when
        PositionManager manager = PositionManager.create(new NewOnesGenerator());
        Throwable thrown = catchThrowable(() -> {
            manager.findAddedRemovedUpdated(actual, updated);
        });

        // then
        assertThat(thrown).hasMessageContaining("In positions in DB there are doubled positionIds");
        assertThat(thrown).isInstanceOf(PositionManagerException.class);
    }

}
