/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.model;

import java.util.List;
import org.seenmodit.model.positions.UpdateOrderPosition;

/**
 *
 * @author jknast
 */
public class UpdateOrderRequest {
    
    private List<UpdateOrderPosition> updateOrderPositions;

    public UpdateOrderRequest(List<UpdateOrderPosition> updateOrderPositions) {
        this.updateOrderPositions = updateOrderPositions;
    }

    public List<UpdateOrderPosition> getUpdateOrderPositions() {
        return updateOrderPositions;
    }

    public void setUpdateOrderPositions(List<UpdateOrderPosition> updateOrderPositions) {
        this.updateOrderPositions = updateOrderPositions;
    }
}
