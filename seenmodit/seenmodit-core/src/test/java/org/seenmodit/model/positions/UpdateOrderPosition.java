/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.model.positions;

import org.seenmodit.core.interfaces.UpdatePosition;

/**
 *
 * @author jknast
 */
public class UpdateOrderPosition implements UpdatePosition{

    private String positionId;
    private String foo;

    public UpdateOrderPosition(String positionId, String foo) {
        this.positionId = positionId;
        this.foo = foo;
    }
        
    @Override
    public String getPositionId() {
        return positionId;
    }

    @Override
    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getFoo() {
        return foo;
    }

    public void setFoo(String foo) {
        this.foo = foo;
    }

    @Override
    public String toString() {
        return "UpdateOrderPosition{" + "positionId=" + positionId + ", foo=" + foo + '}';
    }
}
