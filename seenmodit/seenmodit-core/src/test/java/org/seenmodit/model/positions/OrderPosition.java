/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.model.positions;

import org.seenmodit.core.interfaces.Position;

/**
 *
 * @author jknast
 */
public class OrderPosition implements Position {

    private String positionId;
    private String foo;

    public OrderPosition(String positionId, String foo) {
        this.positionId = positionId;
        this.foo = foo;
    }

    @Override
    public String getPositionId() {
        return positionId;
    }

    @Override
    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getFoo() {
        return foo;
    }

    public void setFoo(String foo) {
        this.foo = foo;
    }

    @Override
    public String toString() {
        return "OrderPosition{" + "positionId=" + positionId + ", foo=" + foo + '}';
    }
}
