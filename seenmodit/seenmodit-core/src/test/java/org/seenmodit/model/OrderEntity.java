/*
* SeEnModIt
* copyright Jakub Knast
* 2018
 */
package org.seenmodit.model;

import org.seenmodit.model.positions.OrderPosition;
import java.util.List;

/**
 *
 * @author jknast
 */
public class OrderEntity {
    
    private List<OrderPosition> orderPositions;

    public OrderEntity(List<OrderPosition> orderPositions) {
        this.orderPositions = orderPositions;
    }

    public List<OrderPosition> getOrderPositions() {
        return orderPositions;
    }

    public void setOrderPositions(List<OrderPosition> orderPositions) {
        this.orderPositions = orderPositions;
    }
    
}
